--P18 from lab 8
--This is an explicit cursor to print out what the pirates have captured. 

DECLARE 
    CURSOR pirateCursor IS SELECT pirate.pirateName, ship.shipName, booty.sailorsCaptured from pirate JOIN battle ON pirate.pirateID = battle.pirateID JOIN booty ON battle.battleID = booty.battleID JOIN ship ON booty.shipID = ship.shipID;
    pirateVar pirateCursor%ROWTYPE;
BEGIN
    OPEN pirateCursor;
    LOOP
    FETCH pirateCursor INTO pirateVar;
    EXIT WHEN pirateCursor%NOTFOUND;
    DBMS_OUTPUT.PUT_LINE(pirateVar.pirateName || ' '|| 'captured the ' || ' ' || pirateVar.shipName || ' ' || 'and' || ' ' || pirateVar.sailorsCaptured || ' ' || ' sailors.'); 
    DBMS_OUTPUT.PUT_LINE(' ');
    END LOOP; 
    CLOSE pirateCursor; 
END; 