--H78 Find the student with the lowest GPA and print them out 

DECLARE 
    CURSOR studentCursor IS SELECT student.firstName, student.lastName, student.studentID from student; -- ORDER BY student.firstName; 
    studentInfo studentCursor%ROWTYPE; 
    CURSOR classCursor IS SELECT enrollment.grade, enrollment.studentID, course.credits from enrollment JOIN section ON enrollment.sectionID = section.sectionID JOIN course ON section.courseID = course.courseID WHERE enrollment.grade <> ' ' AND enrollment.studentID = studentInfo.studentID; 
    classInfo classCursor%ROWTYPE; 
    gradeNumber number(4) := 0; --To convert current grade to a number, then * to credits 
    credit number(4) := 0; -- To count current class credits 
    GPA number(4,2) := 0; -- This has to be higher than a GPA that you can achieve in order to find the lowest. If you set it to 0, no one would have lower than a zero GPA.   
    lowGPA number(4,2) := 0; 
    lowFirst VARCHAR2(20) := 'Junk';
    lowLast VARCHAR2(20) := 'Junk';
    lowID number(4) := 0;
    totalCreditPoints number(4) := 0; -- to save the previous gradeNumber (running total)
    totalCreditNumber number(4) := 0; -- to save the previous credits (running total)
    
BEGIN 
    OPEN studentCursor; 
    LOOP 
    --These Varibles have to be reset everytime that you go to another person, 
    --otherwise they get all of the previous peoples info along with theirs….
    totalCreditPoints := 0; 
    totalCreditNumber := 0;
    GPA := 0; -- This has to be higher than a GPA that you can achieve in order to find the lowest. If you set it to 0, no one would have lower than a zero GPA.
    --This is also because Frodo hasn't completed a class, so it just saves the GPA from the previous person and assigns it to him if you don't reset it before you grab a new person. 
    --Okay now we be good to go fetch another person's info. 
        FETCH studentCursor INTO studentInfo; 
        EXIT WHEN studentCursor%NOTFOUND; 
        OPEN classCursor; 
        LOOP
            FETCH classCursor INTO classInfo; 
            EXIT WHEN classCursor%NOTFOUND;
            IF classInfo.credits = 4 THEN 
                credit := 4;
                ELSIF classInfo.credits = 3 THEN 
                credit := 3;
                ELSIF classInfo.credits = 2 THEN 
                credit := 2;
                ELSIF classInfo.credits = 1 THEN 
                credit := 1;
                END IF; 
                    IF upper(classInfo.grade) = 'A'  THEN
                    gradeNumber := 4 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'B' THEN
                    gradeNumber := 3 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'C' THEN
                    gradeNumber := 2 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'D' THEN
                    gradeNumber := 1 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'F' THEN
                    gradeNumber := 0 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    END IF;
        --DBMS_output.put_line('+++++++++++++++++++++');
        --DBMS_output.put_line('Credit Point: ' || ' ' || gradeNumber);
        --DBMS_output.put_line(' Credits : ' || ' ' || credit);
        totalCreditPoints := gradeNumber + totalCreditPoints; 
        totalCreditNumber := credit + totalCreditNumber;
        GPA := ((totalCreditPoints)/(totalCreditNumber));
        --DBMS_output.put_line(' Total Credit Points : ' || ' ' || totalCreditPoints);
        --DBMS_output.put_line(' Total Credit Number : ' || ' ' || totalCreditNumber);
        --DBMS_output.put_line('GPA : ' || ' ' || GPA);
        END LOOP; 
        IF GPA <= lowGPA THEN 
           lowGPA := GPA; 
           lowFirst := studentInfo.firstName; 
           lowLast := studentInfo.lastName; 
           lowID := studentInfo.studentID;
        END IF; 
       --DBMS_output.put_line('+++++++++++++++++++++');
       --DBMS_output.put_line('Student: ' || ' ' || studentInfo.firstName || ' ' || studentInfo.lastName); 
       --DBMS_output.put_line('Student ID: ' || ' ' || studentInfo.studentID); 
       --GPA := ((totalCreditPoints)/(totalCreditNumber));
       --DBMS_output.put_line('GPA: ' || ' ' || GPA); 
        CLOSE classCursor; 
   END LOOP; 
   DBMS_output.put_line('Here is the student with the lowest GPA: ');
   DBMS_output.put_line('Student: ' || ' ' || lowFirst || ' ' || lowLast); 
   DBMS_output.put_line('Student ID: ' || ' ' || lowID); 
   DBMS_output.put_line('GPA: ' || ' ' || lowGPA);
   CLOSE studentCursor; 
END; 