--H73 Implicit cursor to display course number and section for Summer 2014

DECLARE 
    myCourseNumber course.courseNumber%TYPE; 
    mySection section.section%TYPE; 
    mySectionID section.sectionID%TYPE;
    myCourseName course.CourseName%TYPE; 
BEGIN
    SELECT course.courseNumber, section.section INTO myCourseNumber, mySection from section JOIN course ON section.courseID = course.courseID JOIN term ON section.termID = term.termID WHERE upper(term.description) = 'SUMMER 2014';
    
EXCEPTION
    WHEN TOO_MANY_ROWS THEN 
        dbms_output.put_line('Too many rows bro!!! '); 
    WHEN NO_DATA_FOUND THEN 
        dbms_output.put_line('No rows bro!!! '); 
    WHEN others THEN 
        dbms_output.put_line('Here is what Oracle thinks you did: ' || SQLERRM);
END;
    