--41
select DISTINCT class from student order by class; 

--42
select firstname, middlename, lastname from faculty where middlename is NULL;

--43
select firstname, lastname, to_char(dateofbirth, 'MM-DD-YYYY') 
from student where class = 'JR' or class = 'SR';

--44
select firstname, lastname, dateofbirth 
from student where to_char(dateofbirth, 'YYYY') = '1992';

--45
select * from enrollment where grade is NULL or grade !='F' order by grade;

--46
select sectionid, section, days, ltrim(to_char(time, 'HH12:MI:SS'), '0') as "Time"
from section where instr(days,'M',1,1) > 0;

--47
--The problem with using like '%M%' it would only give you 
--capital M's and not lower case m's.'M%' wouldn't work if it was in the middle
--or if m is lower case. You would also not be able to find it at the end. 
--( if someone wrote in the statement backwards.) M doesn't occur in 
--any other day.... 

--48
select trunc(months_between(sysdate, dateofbirth)/12, 0) 
from student where studentid = '15'; 

--49
select count(grade) from enrollment where sectionid = '36' and grade = 'B';

--50
select avg(maximumenrollment) from section 
where courseid = '51' and termid = '24'