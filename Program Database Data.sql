--Updated tables with primary keys and foreign keys: 

Drop table resourcelocation;
Drop table materiallocation;
Drop table resourceforprogram;
Drop table programresource; 
Drop table sponsorforprogram;
Drop table sponsor;
Drop table building;
Drop table room; 
Drop table program;

create table Building
(
BuildingID number(5) NOT NULL, 
BuildingName varchar2(100),
PRIMARY KEY (BUILDINGID)
);

create table Room 
(
LocationID number(5) NOT NULL, 
BuildingID number(5), 
RoomName varchar2(50),
PRIMARY KEY (LOCATIONID),
constraint Room_fk_buildingID foreign key (buildingID) references building(buildingID)
);

create table Program 
(
ProgramID number(5) NOT NULL, 
ProgramName varchar2(50),
ADApproval char(10), 
Attendance number(5), 
LocationID number(5), 
Cost number(5), 
ProgramDateTime Date,
Notes varchar2(1000),
PRIMARY KEY (ProgramID),
constraint Program_fk_LocationID foreign key (locationID) references room(locationID)
); 

create table Sponsor 
(
SponsorID number(5) not null, 
Name varchar2(100),
BuildingID number(5),
BudgetStart number(5), 
CAorAD varchar2(20),
primary key (sponsorID),
constraint sponsor_fk_buildingID foreign key (buildingID) references building(buildingID)
);

create table SponsorWithProgram
(
SponsorProgramID number(5) NOT NULL, 
ProgramID number(5),
SponsorID number(5), 
CostPerPerson number(5),
PRIMARY KEY (SPONSORPROGRAMID),
constraint swp_fk_programID foreign key (programID) references program(programID),
constraint swp_fk_sponsorID foreign key (sponsorID) references sponsor(sponsorID)
);

create table ResourceforProgram
(
ResourceID number(5) not null,
ResourceName varchar2(50),
LocationID number(5),
SchoolResourceName varchar2(50),
SchoolResourceEmail varchar2(50),
SchoolResourceContactPhone number(10),
primary key (ResourceID), 
constraint rfp_fk_LocationID foreign key (locationID) references room(locationID)
); 

create table ProgramResource
(
ProgramResourceID number(5) not null, 
ProgramID number(5) , 
ResourceID number(5), 
AmountUsed varchar2(50), 
primary key (programresourceID), 
constraint ProgramResource_fk_resourceID foreign key (resourceID) references resourceforprogram(resourceID),
constraint ProgramResource_fk_programID foreign key (programID) references program(programID)
);

create table MaterialLocation
(
MaterialLocationID number(5) not null,
resourceID number(5),
locationID number(5), 
constraint MaterialLocation_fk_resourceID foreign key (resourceID) references resourceforprogram(resourceID),
constraint MaterialLocation_fk_LocationID foreign key (locationID) references room(locationID),
primary key (materiallocationid)
);

create table ResourceLocation
(
ResourceLocationID number(5) not null, 
resourceID number(5), 
LocationID number(5),
constraint ResourceLocation_fk_resourceID foreign key (resourceID) references resourceforprogram(resourceID),
constraint ResourceLocation_fk_LocationID foreign key (locationID) references room(locationID),
primary key (resourcelocationID)
);

--Add data into the tables: 

INSERT INTO BUILDING
values (buildingsequence.nextval, 'Guadalupe Hall');

INSERT INTO BUILDING
values (buildingsequence.nextval, 'St. Charles Hall');

INSERT INTO BUILDING
values (buildingsequence.nextval, 'Borromeo Hall');

INSERT INTO BUILDING
values (buildingsequence.nextval, 'Trinity Hall');

INSERT INTO BUILDING
values (buildingsequence.nextval, 'Simperman Hall');

INSERT INTO BUILDING
values (buildingsequence.nextval, 'Cube');

INSERT INTO BUILDING
values (buildingsequence.nextval, 'Gym');


INSERT INTO room (buildingID, locationID, roomname)
values ((SELECT buildingID from building where upper(buildingName) = 'GUADALUPE HALL'),locationsequence.nextval, 'Guad Lounge');

INSERT INTO room (buildingID, locationID, roomname)
values ((SELECT buildingID from building where upper(buildingName) = 'GUADALUPE HALL'),locationsequence.nextval, 'Guad Rec Room');

INSERT INTO room (buildingID, locationID, roomname)
values ((SELECT buildingID from building where upper(buildingName) = 'GUADALUPE HALL'),locationsequence.nextval, 'Guad Chapel');


INSERT INTO room (buildingID, locationID, roomname)
values ((SELECT buildingID from building where upper(buildingName) = 'TRINITY HALL'),locationsequence.nextval, 'Trinity Lounge');

INSERT INTO room (buildingID, locationID, roomname)
values ((SELECT buildingID from building where upper(buildingName) = 'Borromeo HALL'),locationsequence.nextval, 'Borro Rec Room');


INSERT INTO room (buildingID, locationID, roomname)
values ((SELECT buildingID from building where upper(buildingName) = 'ST. CHARLES HALL'),locationsequence.nextval, 'St. Charles Lounge');

INSERT INTO room (buildingID, locationID, roomname)
values ((SELECT buildingID from building where upper(buildingName) = 'ST. CHARLES HALL'),locationsequence.nextval, 'St. Charles Kitchen');

INSERT INTO program (programID, programname, adapproval, attendance, locationID, cost, programdatetime, notes)
values (programsequence.nextval, 'AMA: Priest Edition', 'Yes', 15, (SELECT ROOMID from room where upper(roomname) = 'GUAD LOUNGE'), 15, to_date('02/22/2016 07:00 PM','MM/DD/YYYY HH:MI AM'), 'This should be a good program!');

INSERT INTO program (programID, programname, adapproval, attendance, locationID, cost, programdatetime, notes)
values (programsequence.nextval, 'AMA: Police Officer Edition', 'Yes', 20, (SELECT ROOMID from room where upper(roomname) = 'GUAD LOUNGE'), 0, to_date('10/20/2015 07:00 PM','MM/DD/YYYY HH:MI AM'), 'This was awesome, Good Job!');

INSERT INTO program (programID, programname, adapproval, attendance, locationID, cost, programdatetime, notes)
values (programsequence.nextval, 'Minute to Win It!', 'Yes', 50, (SELECT ROOMID from room where upper(roomname) = 'GUAD LOUNGE'), 60, to_date('03/17/2016 07:00 PM','MM/DD/YYYY HH:MI AM'), 'So many fun games to play!');

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Tony', (SELECT buildingID from building where upper(buildingName) = 'GUADALUPE HALL'), 140, 'CA'); 

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Hadley', (SELECT buildingID from building where upper(buildingName) = 'GUADALUPE HALL'), 140, 'CA'); 

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Subin', (SELECT buildingID from building where upper(buildingName) = 'GUADALUPE HALL'), 140, 'CA'); 

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Maggie', (SELECT buildingID from building where upper(buildingName) = 'ST. CHARLES HALL'), 140, 'CA'); 

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Carly', (SELECT buildingID from building where upper(buildingName) = 'TRINITY HALL'), 140, 'CA'); 

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Jenni', (SELECT buildingID from building where upper(buildingName) = 'TRINITY HALL'), NULL, 'AD'); 

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Ryan', (SELECT buildingID from building where upper(buildingName) = 'GUADALUPE HALL'), NULL, 'AD'); 

INSERT INTO sponsor (sponsorID, name, buildingID, budgetStart, CAorAD)
values (sponsorsequence.nextval, 'Jake', (SELECT buildingID from building where upper(buildingName) = 'ST. CHARLES HALL'), NULL, 'AD'); 

insert into sponsorwithprogram (sponsorprogramID, programID, sponsorID, costperperson)
values (sponsorwithprogramsequence.nextval, (SELECT programID from program where upper(programName) = 'AMA: PRIEST EDITION'), (SELECT sponsorID from sponsor where upper(name) = 'TONY'), 15); 

insert into sponsorwithprogram (sponsorprogramID, programID, sponsorID, costperperson)
values (sponsorwithprogramsequence.nextval, (SELECT programID from program where upper(programName) = 'AMA: POLICE OFFICER EDITION'), (SELECT sponsorID from sponsor where upper(name) = 'TONY'), 15); 

insert into sponsorwithprogram (sponsorprogramID, programID, sponsorID, costperperson)
values (sponsorwithprogramsequence.nextval, (SELECT programID from program where upper(programName) = 'MINUTE TO WIN IT!'), (SELECT sponsorID from sponsor where upper(name) = 'SUBIN'), 20); 

insert into sponsorwithprogram (sponsorprogramID, programID, sponsorID, costperperson)
values (sponsorwithprogramsequence.nextval, (SELECT programID from program where upper(programName) = 'MINUTE TO WIN IT!'), (SELECT sponsorID from sponsor where upper(name) = 'HADLEY'), 20); 

insert into sponsorwithprogram (sponsorprogramID, programID, sponsorID, costperperson)
values (sponsorwithprogramsequence.nextval, (SELECT programID from program where upper(programName) = 'MINUTE TO WIN IT!'), (SELECT sponsorID from sponsor where upper(name) = 'TONY'), 20); 

INSERT into resourceforprogram (resourceID, resourceName, schoolResourceName, schoolResourceEmail, schoolResourceContactPhone) 
Values (RESOURCEFORPROGRAMSEQUENCE.nextval, 'Rosie Walsh', 'Career Services', 'careers@carroll.edu', 4064475532); 

INSERT into resourceforprogram (resourceID, resourceName, schoolResourceName, schoolResourceEmail, schoolResourceContactPhone) 
Values (RESOURCEFORPROGRAMSEQUENCE.nextval, 'Leslie Olsen', 'Financial Aid', 'lolsen@carroll.edu', 4064475425); 

INSERT into resourceforprogram (resourceID, resourceName, schoolResourceName, schoolResourceEmail) 
Values (RESOURCEFORPROGRAMSEQUENCE.nextval, 'Fr. Marc Lenneman', 'Campus Ministry', 'mlenneman@carroll.edu'); 

INSERT into resourceforprogram (resourceID, resourceName, schoolResourceName, schoolResourceEmail, schoolResourceContactPhone) 
Values (RESOURCEFORPROGRAMSEQUENCE.nextval, 'Kyrie Russ', 'Wellness Center', 'kruss@carroll.edu', 4064475441); 

INSERT into resourceforprogram (resourceID, resourceName, schoolResourceName, schoolResourceEmail, schoolResourceContactPhone) 
Values (RESOURCEFORPROGRAMSEQUENCE.nextval, 'Shannon Ackeret', 'Artaza Center', 'sackeret@carroll.edu', 4064474469); 

insert into programresource (programresourceID, programID, resourceID) 
values (PROGRAMRESOURCESEQUENCE.nextval, (SELECT programID from program where upper(programName) = 'AMA: PRIEST EDITION'), (SELECT resourceID from resourceforprogram where upper(schoolResourceName) = 'CAMPUS MINISTRY'));

--I didn't have a need for my resourceLocation table because I didn't have any sample data for resources that were in multiple locations....
