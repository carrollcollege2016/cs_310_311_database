SELECT 'R4' from dual; 
SELECT pirate.pirateName, ship.shipName, booty.shipID, battle.battletimestamp from pirate JOIN battle on pirate.pirateID = battle.pirateID 
JOIN booty ON battle.battleID = booty.battleID JOIN ship ON booty.shipID = ship.shipID GROUP BY pirate.pirateName, ship.shipName, booty.shipID, battle.battletimestamp  
ORDER BY battle.battletimestamp DESC;

SELECT 'R5' from dual;
SELECT pirate.pirateName, ship.shipName, battle.battletimestamp from booty JOIN ship ON booty.shipID = ship.shipID JOIN battle on booty.battleID = battle.battleID 
RIGHT OUTER JOIN pirate ON battle.pirateID = pirate.pirateID  GROUP BY pirate.pirateName, ship.shipName, booty.shipID, battle.battletimestamp  ORDER BY battle.battletimestamp DESC;

SELECT 'R6' from dual;
SELECT pirate.pirateName, ship.shipName, battle.battletimestamp from booty FULL OUTER JOIN ship ON booty.shipID = ship.shipID 
FULL OUTER JOIN battle on booty.battleID = battle.battleID FULL OUTER JOIN pirate ON battle.pirateID = pirate.pirateID  
GROUP BY pirate.pirateName, ship.shipName, booty.shipID, battle.battletimestamp  ORDER BY battle.battletimestamp DESC;

SELECT 'R7' from dual;
SELECT pirate.pirateName, ship.shipName, battle.battletimestamp from booty FULL OUTER JOIN ship ON booty.shipID = ship.shipID 
FULL OUTER JOIN battle on booty.battleID = battle.battleID FULL OUTER JOIN pirate ON battle.pirateID = pirate.pirateID  WHERE SHIPNAME IS NULL 
ORDER BY battle.battletimestamp DESC;

SELECT 'Number 8' from dual;
SELECT ship.shipName, battle.battletimestamp from booty FULL OUTER JOIN ship ON booty.shipID = ship.shipID 
FULL OUTER JOIN battle on booty.battleID = battle.battleID  
WHERE battle.battletimestamp IS NULL;

SELECT 'R9' from dual;
SELECT ship.shipName, shipcategory.shipcategory from shipcategory JOIN ship ON shipcategory.categoryID = ship.categoryID;

SELECT 'Double digit R10' from dual;
SELECT ship.shipName, shipcategory.shipcategory from shipcategory FULL OUTER JOIN ship ON shipcategory.categoryID = ship.categoryID;

SELECT 'Duplicate 11' from dual;
SELECT DISTINCT shipcategory.shipcategory from shipcategory RIGHT OUTER JOIN ship ON shipcategory.categoryID = ship.categoryID;

SELECT '12th night' from dual;
UPDATE Ship
SET ship.categoryID = (SELECT DISTINCT shipcategory.categoryID from shipcategory where UPPER(shipcategory.shipcategory) = 'TONY''S SHIP')
WHERE SHIP.SHIPID = (SELECT DISTINCT ship.shipID from ship WHERE UPPER(ship.shipName) = 'TONY''S FLAGSHIP');

SELECT 'Lucky13' from dual;
SELECT DISTINCT UPPER(shipcategory.shipcategory) from shipcategory RIGHT OUTER JOIN ship ON shipcategory.categoryID = ship.categoryID;

SELECT 'R14' from dual;
SELECT DISTINCT UPPER(shipcategory.shipcategory) from shipcategory JOIN ship ON shipcategory.categoryID = ship.categoryID JOIN booty ON ship.shipID = booty.shipID;

SELECT 'Half of thirty' from dual;
SELECT Count(ship.shipName) AS "Total Captured Ships" from ship JOIN booty ON ship.shipID = booty.shipID;

SELECT 'R16' from dual;
SELECT Count(DISTINCT ship.shipName) AS "Total Captured Ships" from ship JOIN booty ON ship.shipID = booty.shipID;

SELECT 'R17' from dual;
SELECT Pirate.pirateName, Count(ship.shipName) AS "Total Captured Ships" from ship JOIN booty ON ship.shipID = booty.shipID JOIN battle ON booty.battleID = battle.battleID RIGHT OUTER JOIN 
pirate ON battle.pirateID = pirate.pirateID GROUP BY pirate.pirateName ORDER BY pirate.pirateName;


