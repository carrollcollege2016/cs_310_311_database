--63
DECLARE 
last_name varchar2(20); 
first_name varchar2(20); 
faculty_phone varchar2(20);
BEGIN
last_name := 'Rose'; 
first_name := 'Philbo'; 
faculty_phone := '4064474456';

--SubStr is where you want to start and how far you want to go. 
DBMS_OUTPUT.PUT_LINE(first_name || ' ' || last_name || '''s' || ' ' || 'phone number is' || ' ' || '(' || subStr(faculty_phone, 1, 3 ) || ') ' || subStr(faculty_phone, 4, 3) || '-' || subStr(faculty_phone, 7, 4) ||'.');
END;