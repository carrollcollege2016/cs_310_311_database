--62 
DECLARE 
myFirst student.firstName%TYPE; 
myLast student.lastName%TYPE; 
city student.city%TYPE; 
state student.state%TYPE;
zipcode student.zip%TYPE; 
BEGIN
myFirst := 'Ron'; 
myLast := 'Weasley'; 
city := 'Helena';
state := 'MT'; 
zipcode := 59625;

DBMS_OUTPUT.PUT_LINE(myFirst || ' ' || myLast); 
DBMS_OUTPUT.PUT_LINE(city || ', ' || state || ' ' || zipcode);

END;