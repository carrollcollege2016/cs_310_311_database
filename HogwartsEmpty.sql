-- create HOGWARTS database

--drop tables and start over fresh
DROP TABLE enrollment CASCADE CONSTRAINTS;
DROP TABLE section CASCADE CONSTRAINTS;
DROP TABLE term CASCADE CONSTRAINTS;
DROP TABLE course CASCADE CONSTRAINTS;
DROP TABLE student CASCADE CONSTRAINTS;
DROP TABLE faculty CASCADE CONSTRAINTS;
DROP TABLE location CASCADE CONSTRAINTS;

-- create 7 tables
CREATE TABLE LOCATION
(locationID NUMBER(6),
building VARCHAR2(10),
room VARCHAR2(6),
capacity NUMBER(5), 
CONSTRAINT location_locationID_pk PRIMARY KEY (locationID));

CREATE TABLE faculty
(facultyID NUMBER(6),
lastName VARCHAR2(30),
firstName VARCHAR2(30),
middleName VARCHAR2(30),
locationID NUMBER(5),
phone VARCHAR2(10),
rank VARCHAR2(10),
supervisor NUMBER(6),
image BLOB, 
CONSTRAINT faculty_facultyID_pk PRIMARY KEY(facultyID),
CONSTRAINT faculty_locationID_fk FOREIGN KEY (locationID) REFERENCES location(locationID));

CREATE TABLE student
(studentID NUMBER(6),
lastName VARCHAR2(30),
firstName VARCHAR2(30),
middleName VARCHAR2(30),
address VARCHAR2(25),
city VARCHAR2(20),
state CHAR(2),
zip VARCHAR2(10),
phone VARCHAR2(10),
class CHAR(2),
dateOfBirth DATE,
facultyID NUMBER(6),
CONSTRAINT student_studentID_pk PRIMARY KEY (studentID),
CONSTRAINT student_facultyID_fk FOREIGN KEY (facultyID) REFERENCES faculty(facultyID));

CREATE TABLE TERM
(termID NUMBER(6),
description VARCHAR2(20),
status VARCHAR2(10),
startDate date,
CONSTRAINT term_termID_pk PRIMARY KEY (termID),
CONSTRAINT term_status_cc CHECK ((status = 'OPEN') OR (status = 'CLOSED')));

CREATE TABLE COURSE
(courseID NUMBER(6),
courseNumber VARCHAR2(10),
courseName VARCHAR2(25),
credits NUMBER(2),
CONSTRAINT course_courseID_pk PRIMARY KEY(courseID));

CREATE TABLE SECTION
(sectionID NUMBER(6),
courseID NUMBER(6) CONSTRAINT section_courseID_nn NOT NULL,
section char(1),
termID NUMBER(6) CONSTRAINT section_termID_nn NOT NULL,
facultyID NUMBER(5),
days VARCHAR2(10),
time DATE,
locationID NUMBER(6),
maximumEnrollment NUMBER(4) CONSTRAINT section_maximumEnrollment_nn NOT NULL,
CONSTRAINT section_sectionID_pk PRIMARY KEY (sectionID),
CONSTRAINT section_courseID_fk FOREIGN KEY (courseID) REFERENCES course(courseID), 	
CONSTRAINT section_locationID_fk FOREIGN KEY (locationID) REFERENCES location(locationID),
CONSTRAINT section_termID_fk FOREIGN KEY (termID) REFERENCES term(termID),
CONSTRAINT section_facultyID_fk FOREIGN KEY (facultyID) REFERENCES faculty(facultyID));

CREATE TABLE ENROLLMENT
(studentID NUMBER(6),
sectionID NUMBER(6),
grade CHAR(1),
CONSTRAINT enrollment_pk PRIMARY KEY (studentID, sectionID),
CONSTRAINT enrollment_studentID_fk FOREIGN KEY (studentID) REFERENCES student(studentID),
CONSTRAINT enrollment_sectionID_fk FOREIGN KEY (sectionID) REFERENCES section (sectionID));

commit;