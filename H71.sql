--71 Implicit cursor to display a course name 
DECLARE 
    myCourseName course.courseName%TYPE; 
    mySection section.section%TYPE; 
    mySectionID section.sectionID%TYPE; 
    myRowVar section%ROWTYPE;
BEGIN 
    SELECT course.courseName, section.section, section.sectionID INTO myCourseName, mySection, mySectionID from section JOIN course ON section.courseID = course.courseID WHERE section.sectionID = '31';
    DBMS_output.put_line( myCourseName || ' ' || 'Section' ||' ' || mySection); 
END; 