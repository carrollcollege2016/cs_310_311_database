--65
DECLARE
--don't need to have the (5) with number.
  accumulator NUMBER;
BEGIN
--accumulator was never initialized, so we added 1 to null. 
  accumulator := 0;
  accumulator := accumulator + 1;
  DBMS_OUTPUT.PUT_LINE('The total is ' || TO_CHAR(accumulator));
END;