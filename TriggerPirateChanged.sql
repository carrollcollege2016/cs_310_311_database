CREATE OR REPLACE TRIGGER triggerPirateChanged
before DELETE OR INSERT OR UPDATE ON pirate FOR each ROW
BEGIN
INSERT INTO pirate(oldWhenChanged, newWhenChanged, oldWhoChanged, newWhoChanged)
VALUES(:oldWhenChanged, :newWhenChanged, :oldWhoChanged, v('APP_USER"'));
END;