--66
DECLARE 
current_date DATE;
BEGIN
current_date :=SYSDATE;

IF TO_NUMBER(TO_CHAR(current_date, 'DD')) < 11 THEN
     DBMS_OUTPUT.PUT_LINE('It is day ' || TO_CHAR(current_date, 'DD') || ' of ' || TO_CHAR(current_date, 'Month') || '.' || ' It is early in the month.'); 
ELSIF TO_NUMBER(TO_CHAR(current_date, 'DD')) > 10 AND TO_NUMBER(TO_CHAR(current_date, 'DD')) < 21 THEN
     DBMS_OUTPUT.PUT_LINE('It is day ' || TO_CHAR(current_date, 'DD') || ' of ' || TO_CHAR(current_date, 'Month') || '.' || ' It is the middle of the month.');
ELSIF TO_NUMBER(TO_CHAR(current_date, 'DD')) > 20 AND TO_NUMBER(TO_CHAR(current_date, 'DD')) < 32 THEN
     DBMS_OUTPUT.PUT_LINE('It is day ' || TO_CHAR(current_date, 'DD') || ' of ' || TO_CHAR(current_date, 'Month') || '.' || ' It is nearly the end of the month.');
END IF;
                    
END;