--64
--PL/SQL program to display the current time
DECLARE
   current_time	DATE;
   current_time_char VARCHAR2(30);
BEGIN
   current_time := SYSDATE;
   -- extract time portion from current date
   --the varible was current_tim and not current_time_char.
   --changed to date to to char & set the current_time_char to the current time
   --changed the format to MI.
   current_time_char := TO_CHAR(current_time, 'HH:MI:SS AM');
   --Needed to add a paren to comeplete the output statement.
   DBMS_OUTPUT.PUT_LINE('The time is ' || current_time_char);
END;
