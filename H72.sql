-- H72 Implicit cursor to to display course info for section 31
DECLARE 
    myCourseName course.courseName%TYPE; 
    mySection section.section%TYPE; 
    mySectionID section.sectionID%TYPE; 
    myRowVar section%ROWTYPE;
    myTerm term.description%TYPE; 
    myCourseNumber course.courseNumber%TYPE; 
--Expecting a charter string when converting the time to a char string
    myTime Varchar2(20); 
    myTeacherFirst faculty.firstName%TYPE; 
    myTeacherLast faculty.lastName%TYPE;
    myDays section.days%TYPE; 
BEGIN 
    SELECT term.description, course.CourseNumber, course.courseName, section.days, to_char(section.time, 'HH:MI:SS AM'), faculty.firstName, faculty.Lastname INTO myTerm, myCourseNumber, myCourseName, myDays, myTime, myTeacherFirst, myTeacherLast from section JOIN course ON section.courseID = course.courseID JOIN faculty ON section.facultyID = faculty.facultyID JOIN term ON section.termID = term.termID WHERE section.sectionID = '31';
    DBMS_output.put_line( 'Term: ' || ' ' || myTerm);
    DBMS_output.put_line( 'Course: ' || ' ' || myCourseNumber || ' ' || myCourseName);
    DBMS_output.put_line( 'Time: ' || ' ' || myDays || ' ' || myTime);
    DBMS_output.put_line( 'Instructor: ' || ' ' || myTeacherFirst || ' ' || myTeacherLast);
END; 