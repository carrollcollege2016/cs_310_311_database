--H77
--This calculates the Highest GPA of all the students in the Hogwarts student table. 

DECLARE 
    CURSOR studentCursor IS SELECT student.firstName, student.lastName, student.studentID from student; -- ORDER BY student.firstName; 
    studentInfo studentCursor%ROWTYPE; 
    CURSOR classCursor IS SELECT enrollment.grade, enrollment.studentID, course.credits from enrollment JOIN section ON enrollment.sectionID = section.sectionID JOIN course ON section.courseID = course.courseID WHERE enrollment.grade <> ' ' AND enrollment.studentID = studentInfo.studentID; 
    classInfo classCursor%ROWTYPE; 
    gradeNumber number(4) := 0; --To convert current grade to a number, then * to credits 
    credit number(4) := 0; -- To count current class credits 
    GPA number(4,2) := 0; 
    highGPA number(4,2) := 0; 
    highFirst VARCHAR2(20) := 'Junk';
    highLast VARCHAR2(20) := 'Junk';
    highID number(4) := 0;
    totalCreditPoints number(4) := 0; -- to save the previous gradeNumber (running total)
    totalCreditNumber number(4) := 0; -- to save the previous credits (running total)
    
BEGIN 
    OPEN studentCursor; 
    LOOP 
    --These Varibles have to be reset everytime that you go to another person, 
    --otherwise they get all of the previous peoples info along with theirs….
    totalCreditPoints := 0; 
    totalCreditNumber := 0;
    GPA := 0;
    --Okay now we be good to go fetch another person's info. 
        FETCH studentCursor INTO studentInfo; 
        EXIT WHEN studentCursor%NOTFOUND; 
        OPEN classCursor; 
        LOOP
            FETCH classCursor INTO classInfo; 
            EXIT WHEN classCursor%NOTFOUND;
            IF classInfo.credits = 4 THEN 
                credit := 4;
                ELSIF classInfo.credits = 3 THEN 
                credit := 3;
                ELSIF classInfo.credits = 2 THEN 
                credit := 2;
                ELSIF classInfo.credits = 1 THEN 
                credit := 1;
                END IF; 
                    IF upper(classInfo.grade) = 'A'  THEN
                    gradeNumber := 4 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'B' THEN
                    gradeNumber := 3 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'C' THEN
                    gradeNumber := 2 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'D' THEN
                    gradeNumber := 1 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    ELSIF upper(classInfo.grade) = 'F' THEN
                    gradeNumber := 0 * credit;
                    --DBMS_output.put_line('Grade: ' || ' ' || classInfo.grade);
                    END IF;
        --DBMS_output.put_line('+++++++++++++++++++++');
        --DBMS_output.put_line('Credit Point: ' || ' ' || gradeNumber);
        --DBMS_output.put_line(' Credits : ' || ' ' || credit);
        totalCreditPoints := gradeNumber + totalCreditPoints; 
        totalCreditNumber := credit + totalCreditNumber;
        GPA := ((totalCreditPoints)/(totalCreditNumber));
        --DBMS_output.put_line(' Total Credit Points : ' || ' ' || totalCreditPoints);
        --DBMS_output.put_line(' Total Credit Number : ' || ' ' || totalCreditNumber);
        --DBMS_output.put_line('GPA : ' || ' ' || GPA);
        END LOOP; 
        IF GPA > highGPA THEN 
           highGPA := GPA; 
           highFirst := studentInfo.firstName; 
           highLast := studentInfo.lastName; 
           highID := studentInfo.studentID;
        END IF; 
       --DBMS_output.put_line('+++++++++++++++++++++');
       --DBMS_output.put_line('Student: ' || ' ' || studentInfo.firstName || ' ' || studentInfo.lastName); 
       --DBMS_output.put_line('Student ID: ' || ' ' || studentInfo.studentID); 
       --GPA := ((totalCreditPoints)/(totalCreditNumber));
       --DBMS_output.put_line('GPA: ' || ' ' || GPA); 
        CLOSE classCursor; 
   END LOOP; 
   DBMS_output.put_line('Here is the student with the highest GPA: ');
   DBMS_output.put_line('Student: ' || ' ' || highFirst || ' ' || highLast); 
   DBMS_output.put_line('Student ID: ' || ' ' || highID); 
   DBMS_output.put_line('GPA: ' || ' ' || highGPA);
   CLOSE studentCursor; 
END; 