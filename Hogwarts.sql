-- create HOGWARTS database

--drop tables and start over fresh
DROP TABLE enrollment CASCADE CONSTRAINTS;
DROP TABLE section CASCADE CONSTRAINTS;
DROP TABLE term CASCADE CONSTRAINTS;
DROP TABLE course CASCADE CONSTRAINTS;
DROP TABLE student CASCADE CONSTRAINTS;
DROP TABLE faculty CASCADE CONSTRAINTS;
DROP TABLE location CASCADE CONSTRAINTS;

-- create 7 tables
CREATE TABLE LOCATION
(locationID NUMBER(6),
building VARCHAR2(10),
room VARCHAR2(6),
capacity NUMBER(5), 
CONSTRAINT location_locationID_pk PRIMARY KEY (locationID));

CREATE TABLE faculty
(facultyID NUMBER(6),
lastName VARCHAR2(30),
firstName VARCHAR2(30),
middleName VARCHAR2(30),
locationID NUMBER(5),
phone VARCHAR2(10),
rank VARCHAR2(10),
supervisor NUMBER(6),
image BLOB, 
CONSTRAINT faculty_facultyID_pk PRIMARY KEY(facultyID),
CONSTRAINT faculty_locationID_fk FOREIGN KEY (locationID) REFERENCES location(locationID));

CREATE TABLE student
(studentID NUMBER(6),
lastName VARCHAR2(30),
firstName VARCHAR2(30),
middleName VARCHAR2(30),
address VARCHAR2(25),
city VARCHAR2(20),
state CHAR(2),
zip VARCHAR2(10),
phone VARCHAR2(10),
class CHAR(2),
dateOfBirth DATE,
facultyID NUMBER(6),
CONSTRAINT student_studentID_pk PRIMARY KEY (studentID),
CONSTRAINT student_facultyID_fk FOREIGN KEY (facultyID) REFERENCES faculty(facultyID));

CREATE TABLE TERM
(termID NUMBER(6),
description VARCHAR2(20),
status VARCHAR2(10),
startDate date,
CONSTRAINT term_termID_pk PRIMARY KEY (termID),
CONSTRAINT term_status_cc CHECK ((status = 'OPEN') OR (status = 'CLOSED')));

CREATE TABLE COURSE
(courseID NUMBER(6),
courseNumber VARCHAR2(10),
courseName VARCHAR2(25),
credits NUMBER(2),
CONSTRAINT course_courseID_pk PRIMARY KEY(courseID));

CREATE TABLE SECTION
(sectionID NUMBER(6),
courseID NUMBER(6) CONSTRAINT section_courseID_nn NOT NULL,
section char(1),
termID NUMBER(6) CONSTRAINT section_termID_nn NOT NULL,
facultyID NUMBER(5),
days VARCHAR2(10),
time DATE,
locationID NUMBER(6),
maximumEnrollment NUMBER(4) CONSTRAINT section_maximumEnrollment_nn NOT NULL,
CONSTRAINT section_sectionID_pk PRIMARY KEY (sectionID),
CONSTRAINT section_courseID_fk FOREIGN KEY (courseID) REFERENCES course(courseID), 	
CONSTRAINT section_locationID_fk FOREIGN KEY (locationID) REFERENCES location(locationID),
CONSTRAINT section_termID_fk FOREIGN KEY (termID) REFERENCES term(termID),
CONSTRAINT section_facultyID_fk FOREIGN KEY (facultyID) REFERENCES faculty(facultyID));

CREATE TABLE ENROLLMENT
(studentID NUMBER(6),
sectionID NUMBER(6),
grade CHAR(1),
CONSTRAINT enrollment_pk PRIMARY KEY (studentID, sectionID),
CONSTRAINT enrollment_studentID_fk FOREIGN KEY (studentID) REFERENCES student(studentID),
CONSTRAINT enrollment_sectionID_fk FOREIGN KEY (sectionID) REFERENCES section (sectionID));


---- insert records into LOCATION table
INSERT INTO location (locationID, building, room, capacity) VALUES
(1, 'CASTLE', '101', 100);

INSERT INTO location (locationID, building, room, capacity) VALUES
(2, 'CASTLE', '106', 40);

INSERT INTO location (locationID, building, room, capacity) VALUES
(3, 'CASTLE', '108', 35);

INSERT INTO location (locationID, building, room, capacity) VALUES
(4, 'CASTLE', '110', 35);

INSERT INTO location (locationID, building, room, capacity) VALUES
(5, 'CASTLE', '114', 50);

INSERT INTO location (locationID, building, room, capacity) VALUES
(6, 'CASTLE', '109-C', 1);

INSERT INTO location (locationID, building, room, capacity) VALUES
(7, 'CASTLE', '109-D', 2);

INSERT INTO location (locationID, building, room, capacity) VALUES
(8, 'CASTLE', '109-F', 1);

INSERT INTO location (locationID, building, room, capacity) VALUES
(9, 'QUIDDITCH', '120', 25);

INSERT INTO location (locationID, building, room, capacity) VALUES
(10, 'QUIDDITCH', '123', 12);

INSERT INTO location (locationID, building, room, capacity) VALUES
(11, 'GUAD', '101', 40);

INSERT INTO location (locationID, building, room, capacity) VALUES
(12, 'GUAD', '102', 2);


--- insert records into FACULTY table
INSERT INTO faculty (facultyID, lastName, firstName, middleName, locationID, phone, rank, supervisor,  image) VALUES
(1, 'Grey', 'Gandalf', 'the', 7, '4064474455', 'Professor', NULL, EMPTY_BLOB());

INSERT INTO faculty  (facultyID, lastName, firstName, middleName, locationID, phone, rank, supervisor,  image) VALUES
(2, 'Dumbledore', 'Albus', NULL, 6, '4064474466', 'Professor', NULL, EMPTY_BLOB());

INSERT INTO faculty  (facultyID, lastName, firstName, middleName, locationID, phone, rank, supervisor,  image) VALUES
(3, 'Hagrid', 'Rubeus', NULL, 12, '4064474477', 'Instructor', 2, EMPTY_BLOB());

INSERT INTO faculty  (facultyID, lastName, firstName, middleName, locationID, phone, rank, supervisor,  image) VALUES
(4, 'Arathorn', 'Aragorn', 'Ben', 12, '4064475151', 'Asst. Prof', 1, EMPTY_BLOB());

INSERT INTO faculty  (facultyID, lastName, firstName, middleName, locationID, phone, rank, supervisor,  image) VALUES
(5, 'Snape', 'Severus', NULL, 8, '4064474456', 'Asst. Prof', 2, EMPTY_BLOB());

--- insert records into STUDENT table
INSERT INTO student (studentID, lastName, firstName, middleName, address, city, state, zip, phone, class, dateOfBirth,  facultyID)  VALUES
(11, 'Potter', 'Harry', NULL, '4 Privet Drive', 'Little Whinging', 'MT', '59629', '4064421234', 'JR', TO_DATE('02/17/1992', 'MM/DD/YYYY'), 5);

INSERT INTO student  (studentID, lastName, firstName, middleName, address, city, state, zip, phone, class, dateOfBirth,  facultyID)   VALUES
(12, 'Granger', 'Hermione', NULL, '23 W. Muggle Dr', 'London',  'MA', '12549', '813562586', 'JR', TO_DATE('04/28/1992', 'MM/DD/YYYY'), 5);

INSERT INTO student (studentID, lastName, firstName, middleName, address, city, state, zip, phone, class, dateOfBirth,  facultyID)  VALUES
(13, 'Baggins', 'Bilbo', NULL, '14 Middle Earth', 'Helena', 'MT', '59625', '4064431313', 'SR', TO_DATE('06/06/1991', 'MM/DD/YYYY'),  1);

INSERT INTO student (studentID, lastName, firstName, middleName, address, city, state, zip, phone, class, dateOfBirth,  facultyID)  VALUES
(14, 'Baggins', 'Frodo', NULL, '14 Middle Earth', 'Helena', 'MT', '59625', '4064431313', 'SO', TO_DATE('07/07/1993', 'MM/DD/YYYY'),  1);

INSERT INTO student (studentID, lastName, firstName, middleName, address, city, state, zip, phone, class, dateOfBirth,  facultyID)  VALUES
(15, 'Weasley', 'Ginny', 'Q', '12 Main', 'Cascade', 'CA', '98765', '9193332222', 'FR', TO_DATE('11/20/1994', 'MM/DD/YYYY'), 2);


--- insert records into TERM table
INSERT INTO term (termID, description, status, startDate) VALUES
(21, 'Fall 2012', 'CLOSED', TO_DATE('08/27/2012', 'MM/DD/YYYY') );

INSERT INTO term (termID, description, status, startDate) VALUES
(22, 'Spring 2013', 'OPEN', TO_DATE('01/10/2013', 'MM/DD/YYYY') );

INSERT INTO term (termID, description, status, startDate) VALUES
(23, 'Summer 2013', 'OPEN', TO_DATE('05/10/2013', 'MM/DD/YYYY') );

INSERT INTO term (termID, description, status, startDate) VALUES
(24, 'Fall 2013', 'CLOSED', TO_DATE('01/11/2013', 'MM/DD/YYYY') );

INSERT INTO term (termID, description, status, startDate) VALUES
(25, 'Spring 2014', 'CLOSED', TO_DATE('08/29/2014', 'MM/DD/YYYY') );

INSERT INTO term (termID, description, status, startDate) VALUES
(26, 'Summer 2014', 'CLOSED', TO_DATE('05/11/2014
', 'MM/DD/YYYY') );

--- insert records into COURSE table
INSERT INTO course (courseID, courseNumber, courseName, credits) VALUES
(51, 'FL 101', 'Flying', 4);

INSERT INTO course (courseID, courseNumber, courseName, credits) VALUES
(52, 'HE 202', 'Herbology', 3);

INSERT INTO course (courseID, courseNumber, courseName, credits) VALUES
(53, 'DA 330', 'Defense Ag. the Dark Arts', 3);

INSERT INTO course (courseID, courseNumber, courseName, credits) VALUES
(54, 'EX 222', 'Muggle Studies', 2);

INSERT INTO course (courseID, courseNumber, courseName, credits) VALUES
(55, 'EX 303', 'Transfiguration', 4);


--- insert records into SECTION table
INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (31, 51, 'A', 24, 2, 'MWF', TO_DATE('10:00 AM', 'HH:MI AM'), 1, 98);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (32, 51, 'B', 24, 3, 'TuTh', TO_DATE('09:30 AM', 'HH:MI AM'),  7, 35);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (33, 51, 'C', 24,3, 'MWF', TO_DATE('10:00 AM', 'HH:MI AM'), 2, 35);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (34, 52, 'A', 24, 4, 'TuTh', TO_DATE('11:00 AM', 'HH:MI AM'), 9, 30);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (35, 52, 'B', 25, 4, 'TuTh', TO_DATE('02:00 PM', 'HH:MI PM'), 9, 35);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (36, 53, 'A', 25, 1, 'MWF', TO_DATE('10:00 AM', 'HH:MI AM'), 5, 30);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (37, 53, 'B', 25, 1, 'MWF', TO_DATE('10:00 AM', 'HH:MI AM'), 5, 30);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment)  
VALUES (38, 54, 'A', 25, 5, 'TuTh', TO_DATE('11:00 AM', 'HH:MI AM'),  3, 22);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (39, 55, 'A', 25, 2, 'MWF', TO_DATE('02:00 PM', 'HH:MI PM'),  5, 19);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (40, 55, 'B', 25, 2, 'MWF', TO_DATE('03:00 PM', 'HH:MI PM'), 5, 35);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (41, 51, 'A', 26, 1, 'MTuWThF', TO_DATE('08:00 AM', 'HH:MI AM'), 1, 5);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (42, 52, 'A', 26, 2, 'MTuWTh', TO_DATE('08:00 AM', 'HH:MI AM'),  9, 28);

INSERT INTO section (sectionID, courseID, section, termID, facultyID, days, time, locationID, maximumEnrollment) 
VALUES (43, 53, 'A', 26, 3, 'MTuWThF', TO_DATE('09:00 AM', 'HH:MI AM'),  5, 35);

--- insert records into ENROLLMENT table
INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(11, 31, 'A');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(11, 34, 'A');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(11, 36, 'B');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(11, 39, 'B');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(12, 31, 'C');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(12, 35, 'B');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(12, 36, 'A');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(12, 39, 'B');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(13, 31, 'C');

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(13, 42, NULL);

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(13, 43, NULL);

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(14, 41, NULL);

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(14, 42, NULL);

INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(15, 31, 'B');
INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(15, 34, 'F');
INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(15, 36, 'F');
INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(15, 35, 'C');
INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(15, 39, 'C');
INSERT INTO enrollment (studentID, sectionID, grade) VALUES
(15, 43, NULL);


COMMIT;
