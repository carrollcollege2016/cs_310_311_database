--51
SELECT DISTINCT term.termId, course.courseID, course.courseName from section JOIN Course ON section.courseId = course.courseId JOIN term ON section.termId = term.termId where term.description = 'Fall 2013';

--52 Join needs to have the table name, not column name.
SELECT DISTINCT student.firstName, student.lastName from student JOIN faculty ON student.facultyId = faculty.facultyId where faculty.firstName = 'Severus';

--53
SELECT count(DISTINCT enrollment.studentId) "Current Enrollment" from enrollment JOIN section ON enrollment.sectionID = section.sectionId JOIN term ON section.termId = term.termId where term.Description = 'Fall 2013'; 
--SELECT count(DISTINCT enrollment.studentId), enrollment.sectionId from enrollment
--JOIN section ON enrollment.sectionID = section.sectionId JOIN term 
--ON section.termId = term.termId where term.Description = 'Fall 2013'; 

--54
SELECT DISTINCT course.courseName, term.description, enrollment.grade from section JOIN term ON section.termId = term.termID JOIN course ON section.courseID = course.courseId JOIN enrollment ON section.sectionId = enrollment.sectionID JOIN student ON enrollment.studentId = student.studentId where student.firstName = 'Harry';

--55
SELECT SUM(course.credits) "Credit Total" from course JOIN section ON course.courseId = section.courseId JOIN enrollment ON section.sectionId = enrollment.sectionId JOIN student ON enrollment.studentID = student.studentID where student.firstName = 'Bilbo' AND enrollment.grade IS NOT NULL;

--56
SELECT count(DISTINCT student.studentId) "Who dat Hagrid"from student JOIN faculty ON student.facultyId = faculty.facultyID JOIN section ON faculty.facultyId = section.facultyID JOIN term ON section.termID = term.termID where term.description = 'Spring 2013' AND faculty.firstName = 'Hagrid';

--57
SELECT SUM(course.credits) "Credit Total for Fall" from course JOIN section ON course.courseId = section.courseId JOIN enrollment ON section.sectionId = enrollment.sectionId JOIN student ON enrollment.studentID = student.studentID JOIN term ON section.termID = term.termID where enrollment.grade IS NOT NULL AND term.description = 'Fall 2013';

--58
SELECT term.description, course.courseName, section.section, location.building, location.room from section JOIN term ON section.termID = term.termID JOIN course ON section.courseID = course.courseID JOIN location ON section.locationId = location.locationID JOIN faculty ON section.facultyID = faculty.facultyId where faculty.firstName = 'Albus';

--59
SELECT course.courseID, TO_CHAR(section.time, 'HH:MI:SS AM') "Time", section.days from section JOIN course ON section.courseID = course.courseID JOIN term ON section.termID = term.termID JOIN enrollment ON section.sectionID = enrollment.sectionID JOIN student ON enrollment.studentID = student.studentID where Student.firstName = 'Frodo' AND term.description = 'Summer 2014' ORDER BY section.days;
--60
SELECT location.building, location.room FROM location WHERE location.capacity >=(SELECT location.capacity FROM location WHERE location.building = upper('Castle') AND location.room = '106');

--61
SELECT location.building, location.room, to_char((section.time), 'HH:MI') FROM location LEFT OUTER JOIN section ON section.locationID = location.locationID ORDER BY location.building, location.room;

--62
SELECT location.building, location.room, to_char(section.time, 'HH:MI'),section.section, term.description, section.courseID FROM location LEFT OUTER JOIN section ON section.locationID = location.locationID LEFT OUTER JOIN term ON section.termID = term.termID ORDER BY location.building, location.room;

--63
SELECT location.building, location.room, to_char(section.time, 'HH:MI') "Time" ,section.section, term.description "Term", section.courseID "Class Number" FROM section JOIN term ON section.termID = term.termID AND upper(term.description) = 'FALL 2013' RIGHT OUTER JOIN location ON section.locationID = location.locationID  ORDER BY location.building, location.room;

--64 is --49 from last week! Crazy!! 
--This gives me a zero instead of a one like it is supposed to.... Needs to change courseName to courseNumber....
SELECT count(DISTINCT enrollment.grade) "Number of B's" FROM enrollment JOIN section ON enrollment.sectionID = section.sectionID JOIN course ON section.courseID = course.courseID JOIN term ON section.termID = term.termID WHERE  upper(course.courseNumber) = 'DA 330' AND upper(term.Description) = 'SPRING 2014' AND upper(enrollment.grade) = 'B';

--65 is --50 from last week! even Crazier!!!
SELECT AVG(section.maximumenrollment) "Max Enrollment" FROM section JOIN course ON section.courseID = course.courseID JOIN term ON section.termID = term.termID WHERE UPPER(course.courseNumber) = 'FL 101' AND UPPER(term.Description) = 'FALL 2013'; 


