--H75 
-- Using Explicit cursor
-- cursor to display course number and section for Summer 2014

DECLARE 
    CURSOR myCursor IS SELECT course.courseNumber, section.section, section.sectionID from section JOIN course ON section.courseID = course.courseID JOIN term ON section.termID = term.termID WHERE upper(term.description) = 'SUMMER 2014';
    myRowVar myCursor%ROWTYPE; 
    
    Cursor studentCursor IS SELECT student.firstName, student.lastName, enrollment.sectionID, section.termID, course.courseNumber from student JOIN enrollment ON student.studentID = enrollment.studentID JOIN section ON enrollment.sectionID = section.sectionID JOIN term ON section.termID = term.termID JOIN course ON section.courseID = course.courseID WHERE section.sectionID = myRowVar.SectionID;
    
    myStudentVar studentCursor%ROWTYPE; 
    
    --myCourseNumber course.courseNumber%TYPE; 
    --mySection section.section%TYPE; 
    --mySectionID section.sectionID%TYPE;
   -- myCourseName course.CourseName%TYPE; 
   
BEGIN
    Open myCursor;
    LOOP 
        FETCH myCursor INTO myRowVar; 
        EXIT WHEN myCursor%NOTFOUND; 
        dbms_output.put_line('=================================');
                dbms_output.put_line(myRowVar.courseNumber || ' ' || 'Section' || ' ' || myRowVar.section);
                dbms_output.put_line('----------------------------------');
        OPEN studentCursor;
            LOOP 
                FETCH studentCursor INTO myStudentVar; 
                EXIT WHEN studentCursor%NOTFOUND; 
                
                dbms_output.put_line(myStudentVar.firstName || ' ' || myStudentVar.LastName); 
            END LOOP; 
            CLOSE studentCursor;
            
    --dbms_output.put_line('Here is the course number: ' || ' ' || myRowVar.courseNumber || ' ' || 'and the section: ' || ' ' || myRowVar.section || ' ' || 'for the Summer 2014 term.');
    END LOOP;
    CLOSE myCursor;
    
EXCEPTION
    WHEN TOO_MANY_ROWS THEN 
       dbms_output.put_line('Too many rows bro!!! '); 
    WHEN NO_DATA_FOUND THEN 
        dbms_output.put_line('No rows bro!!! '); 
    WHEN others THEN 
        dbms_output.put_line('Here is what Oracle thinks you did: ' || SQLERRM);
END; 