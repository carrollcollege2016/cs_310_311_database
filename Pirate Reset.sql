-- delete data, rather than drop tables since have added a field or two
delete from booty;
delete from battle;
delete from pirate;
delete from ship;
delete from shipCategory;
insert into shipCategory(categoryID, shipCategory) values (101, 'Sailing Ship');
insert into shipCategory(categoryID, shipCategory) values (102, 'Convict Ship');
insert into shipCategory(categoryID, shipCategory) values (103, 'Battleship');
insert into shipCategory(categoryID, shipCategory) values (104, 'Ship in a Bottle');


insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (31,'Pearl','Right out of an oyster',101,'Y',1200 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (32,'Margaret','Smallish ship',104,'Y',199 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (33,'Rising Sun','Deadly ship',102,'Y',40 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (34,'Queen Annes Revenge','Beware',102,'Y',250 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (35,'Royal Fortune','Fighting boat',103,'Y',500 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (36,'Royal James','Fast sailer',101,'Y',200 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (37,'Bismarck','A Monster',103,'Y',3995 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (38,'William and Mary','Ready for action',103,'Y',50 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (39,'Queen Victoria','Plenty of sails',101,'Y',1999 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (40,'Neptune','Amazing sailing skills',104,'Y',150 );
insert into ship (shipID, shipName, shipDescription, categoryID, isOutOfHarbor, capacity) values (41,'Never Be Taken Alive','One Tough Ship',102,'Y',666);



insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (1, 'Black Beard', 'English', 'Polly', '2001 Space Odyssey', 'bbeard@yahoo.com' , 1000);
insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (2, 'Jane de Belleville',  'French', 'Pierre', '20,000 Leagues under Sea', 'weewee@fr.net', 100);
insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (3, 'Captain Hook', 'English',  'Tinker Belle',  'Hooks Revenge', 'gator@disney.com',11);
insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (4, 'Grace Omalley','Irish', 'Gracie',  'When Irish Eyes Are Smilin',  'gg@yahoo.com', 90);
insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (5, 'Red Beard',  'Irish', 'Polly','Ol Yeller',  'redred@red.com', 90);
insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (6, 'Rachel Wall',  'American',  'Polly', 'American Woman',  'whyMe@google.com',100);
insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (7, 'Jack Sparrow',  'English', 'Polly', 'Pirates of the Caribbean', 'butyouhaveheardofme@yahoo.com',90);
insert into pirate (pirateID, pirateName, nationality, parrot, favoriteMovie, email, maxSparedPerShip) values (8, 'Johnny Stay At Home',  'English', 'None', 'None', 'no email',1);



insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (51, 'DEC-02-1492', 1, 10);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (52, 'NOV-28-1493', 2, 20);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (53, 'NOV-23-1542', 2, 30);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (54, 'NOV-18-1598', 3, 40);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (55, 'NOV-13-1612', 3, 50);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (56, 'JAN-08-2013', 4, 60);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (57, 'FEB-03-2013', 5, 70);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (58, 'MAR-27-2013', 6, 80);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (59, 'SEP-22-2012', 7, 90);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (60, 'OCT-17-2012', 7, 100);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (61, 'NOV-02-2012', 1, 101);
insert into battle (battleID, battleTimestamp, pirateID, sparedInBattle) values (62, 'DEC-02-2012', 1, 102);



insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (1, 51, 31, 1200, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (2, 57, 31, 1100, 30);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (3, 56, 31, 1300, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (4, 52, 32, 199, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (5, 55, 33, 30, 50);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (6, 58, 33, 40, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (7, 58, 34, 250, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (8, 54, 34, 250, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (9, 56, 34, 350, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (10, 59, 35, 300, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (11, 56, 35, 350, 30);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (12, 54, 35, 500, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (13, 60, 35, 700, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (14, 59, 36, 200, 50);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (15, 55, 37, 3995, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (16, 55, 38, 50, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (17, 52, 38, 55, 50);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (18, 57, 38, 50, 25);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (19, 53, 39, 2200, 100);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (20, 56, 40, 175, 50);
insert into booty(bootyID, battleID, shipID, sailorsCaptured, percentSpared) values (21, 52, 40, 150, 30);

commit;